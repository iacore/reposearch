//! now used mostly for debugging

use color_eyre::Report;
use reposearch::*;
use tracing_error::ErrorLayer;
use tracing_subscriber::prelude::*;

#[tokio::main]
async fn main() -> Result<(), Report> {
    color_eyre::install()?;

    let subscriber = tracing_subscriber::Registry::default()
        .with(ErrorLayer::default())
        .with(
            tracing_subscriber::fmt::layer()
                .with_filter(tracing_subscriber::EnvFilter::from_default_env()),
        );

    subscriber.try_init()?;

    // let data = github_search_repo("boo", 1).await?;
    // let data = srht::sourcehut_search_repos("boo", 1, srht::SourcehutSortBy::RecentlyUpdated).await?;
    let data = forgejo::search_repos(&forgejo::APIBASE_CODEBERG, "code", 1).await?;
    println!("{:?}", data);
    Ok(())
}
