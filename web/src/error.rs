use std::fmt::Write;

use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};

/// dummy type that provides error report
pub struct AppError(pub eyre::Error, pub crate::SearchParams);

impl IntoResponse for AppError {
    fn into_response(self) -> Response {
        tracing::error!("{:?}", self.0);
        let mut error_message: String = "Something went wrong:\n".into();
        for reason in self.0.chain() {
            writeln!(&mut error_message, "- {}", reason).unwrap();
        }
        (
            StatusCode::INTERNAL_SERVER_ERROR,
            crate::TmplError { nav: self.1, error_message },
        ).into_response()
    }
}
