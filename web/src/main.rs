use std::{
    fs::{self, Permissions},
    net::SocketAddr,
    os::unix::prelude::PermissionsExt,
    path::PathBuf,
};

use axum::{extract::Query, routing::get, Router};
use eyre::{Context, ContextCompat};
use hyperlocal::UnixServerExt;
use reposearch::forgejo;
use url::Url;

use reposearch_web::*;

#[tokio::main]
async fn main() -> Result<(), eyre::Report> {
    color_eyre::install()?;
    use tracing_subscriber::prelude::*;
    tracing_subscriber::Registry::default()
        .with(tracing_error::ErrorLayer::default())
        .with(
            tracing_subscriber::fmt::layer()
                .with_filter(tracing_subscriber::EnvFilter::from_default_env()),
        )
        .try_init()?;

    // build our application with a route
    let app = Router::new()
        .route("/", get(root))
        .route("/search", get(route_search))
        .fallback_service(tower_http::services::ServeDir::new("static"));

    // run our app with hyper
    // `axum::Server` is a re-export of `hyper::Server`
    match std::env::var("PORT") {
        Ok(port_str) => {
            if port_str.contains("/") {
                let path: PathBuf = port_str.into();
                serve_unix(app, path).await
            } else {
                let port = port_str.parse().context(
                    "PORT should be a number or contains '/' to be considered UNIX path",
                )?;
                let addr = SocketAddr::from(([127, 0, 0, 1], port));
                serve_tcp(app, addr).await
            }
        }
        Err(_) => {
            eprintln!("No PORT specified. Default to 127.0.0.1:3000.");
            let addr = SocketAddr::from(([127, 0, 0, 1], 3000));
            serve_tcp(app, addr).await
        }
    }
}

async fn serve_tcp(app: Router, addr: SocketAddr) -> Result<(), eyre::Report> {
    eprintln!("listening on http://{}/", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await?;
    Ok(())
}

async fn serve_unix(app: Router, path: PathBuf) -> Result<(), eyre::Report> {
    if path.exists() {
        fs::remove_file(&path)?;
    }

    let server = axum::Server::bind_unix(&path)?; // bind socket first

    fs::set_permissions(&path, Permissions::from_mode(0o660))?;

    eprintln!("listening on {:?}", &path);
    server.serve(app.into_make_service()).await?;
    Ok(())
}

async fn root() -> TmplIndex {
    TmplIndex::default()
}

async fn route_search(Query(params): Query<SearchParams>) -> Result<TmplSearch, AppError> {
    let page = params.page.unwrap_or(1);
    let query = params.q.clone();

    let provider = params
        .provider
        .context("should specify provider")
        .map_err(|e| AppError(e.into(), params.clone()))?;

    // todo: move this part to lib. This is not coupled to the HTTP server
    let (search_results, max_page) = match provider {
        SearchProvider::GitHub => {
            let data = reposearch::github::github_search_repos(&query, page)
                .await
                .map_err(|e| AppError(e.into(), params.clone()))?;

            let max_page = data.max_page();

            let results = data
                .items
                .into_iter()
                .map(|x| SearchResult {
                    name: x.full_name,
                    description: x.description,
                    html_url: x.html_url.into(),
                    stars: Some(x.stargazers_count),
                    updated_at: x.updated_at,
                    tags: None,
                })
                .collect();
            (results, Some(max_page))
        }
        SearchProvider::Dummy => (
            vec![SearchResult {
                name: "foo/bar".into(),
                description: Some("Placeholder".into()),
                html_url: Url::parse("http://example.com/").unwrap(),
                stars: Some(42),
                updated_at: Some(std::time::SystemTime::now().into()),
                tags: None,
            }],
            None,
        ),
        SearchProvider::Sourcehut => {
            let data = reposearch::srht::sourcehut_search_repos(
                &query,
                page,
                reposearch::srht::SourcehutSortBy::RecentlyUpdated,
            )
            .await
            .map_err(|e| AppError(e.into(), params.clone()))?;
            let results = data
                .repos
                .into_iter()
                .map(|repo| SearchResult {
                    name: repo.full_name(),
                    description: repo.description,
                    html_url: repo.repo_url,
                    stars: None,
                    updated_at: None,
                    tags: Some(repo.tags),
                })
                .collect();
            (results, Some(data.max_page))
        }
        SearchProvider::Forgejo => {
            let data = forgejo::search_repos(
                params
                    .forgejo_endpoint
                    .as_ref()
                    .unwrap_or(&forgejo::APIBASE_CODEBERG),
                &query,
                page,
            )
            .await
            .map_err(|e| AppError(e.into(), params.clone()))?;
            let results = data
                .data
                .into_iter()
                .map(|repo| SearchResult {
                    name: repo.full_name,
                    description: Some(repo.description),
                    html_url: repo.html_url,
                    stars: Some(repo.stars_count),
                    updated_at: Some(repo.updated_at),
                    tags: None,
                })
                .collect();
            (results, None)
        }
    };

    Ok(TmplSearch {
        nav: params.clone(),
        max_page,
        columns: provider.into(),
        search_results_json: serde_json::to_value(&search_results)
            .map_err(|e| AppError(e.into(), params))?,
        search_results,
    })
}
