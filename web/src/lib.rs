use chrono::DateTime;
use chrono_humanize::HumanTime;
use reposearch::srht::SourcehutTag;
use serde::{Deserialize, Serialize};
use serde_json::Value as JSONValue;
use strum::IntoEnumIterator;
use url::Url;

pub mod error;

pub use crate::error::AppError;

// this is case sensitive
#[derive(Clone, Copy, PartialEq, Eq, Deserialize, Serialize, strum::EnumIter, strum::Display)]
#[serde(rename_all = "lowercase")]
pub enum SearchProvider {
    GitHub,
    Sourcehut,
    Forgejo,
    Dummy,
}

/// url query part
#[derive(Clone, Deserialize)]
pub struct SearchParams {
    pub q: String,
    #[serde(deserialize_with = "case_insensitive")]
    pub provider: Option<SearchProvider>,
    pub page: Option<u64>,
    #[serde(deserialize_with = "empty_string_as_none")]
    pub forgejo_endpoint: Option<Url>,
}

impl Default for SearchParams {
    fn default() -> Self {
        Self {
            q: "".into(),
            page: Some(1),
            provider: None,
            forgejo_endpoint: None,
        }
    }
}

#[derive(Clone, Copy)]
pub struct EnabledColumns {
    pub stars: bool,
    pub updated_at: bool,
    pub tags: bool,
}
impl From<SearchProvider> for EnabledColumns {
    fn from(value: SearchProvider) -> Self {
        match value {
            SearchProvider::GitHub => EnabledColumns {
                stars: true,
                updated_at: true,
                tags: false,
            },
            SearchProvider::Dummy => EnabledColumns {
                stars: true,
                updated_at: true,
                tags: false,
            },
            SearchProvider::Sourcehut => EnabledColumns {
                stars: false,
                updated_at: false,
                tags: true,
            },
            SearchProvider::Forgejo => EnabledColumns {
                stars: true,
                updated_at: true,
                tags: false,
            },
        }
    }
}

#[derive(Serialize)]
pub struct SearchResult {
    pub name: String,
    pub description: Option<String>,
    pub html_url: Url,
    pub stars: Option<u64>,
    pub updated_at: Option<DateTime<chrono::Utc>>,
    pub tags: Option<Vec<SourcehutTag>>,
}

impl SearchResult {
    pub fn updated_at_str(&self) -> Option<HumanTime> {
        let now = DateTime::from(std::time::SystemTime::now());
        self.updated_at
            .map(|updated_at| HumanTime::from(updated_at - now))
    }
}

#[derive(Default, askama::Template)]
#[template(path = "index.html")]
pub struct TmplIndex {
    pub nav: SearchParams,
}

#[derive(askama::Template)]
#[template(path = "search.html")]
pub struct TmplSearch {
    pub nav: SearchParams,
    pub columns: EnabledColumns,
    pub search_results: Vec<SearchResult>,
    pub search_results_json: JSONValue,
    pub max_page: Option<u64>,
}

#[derive(askama::Template)]
#[template(path = "error.html")]
pub struct TmplError {
    pub nav: SearchParams,
    pub error_message: String,
}

fn empty_string_as_none<'de, D, T>(de: D) -> Result<Option<T>, D::Error>
where
    D: serde::Deserializer<'de>,
    T: serde::Deserialize<'de>,
{
    let opt = Option::<String>::deserialize(de)?;
    let opt = opt.as_ref().map(String::as_str);
    match opt {
        None | Some("") => Ok(None),
        Some(s) => T::deserialize(serde::de::IntoDeserializer::into_deserializer(s)).map(Some),
    }
}

fn case_insensitive<'de, D, T>(de: D) -> Result<Option<T>, D::Error>
where
    D: serde::Deserializer<'de>,
    T: serde::Deserialize<'de>,
{
    let opt = Option::<String>::deserialize(de)?;
    let opt = opt.as_ref().map(String::as_str);
    match opt {
        None => Ok(None),
        Some(s) => T::deserialize(serde::de::IntoDeserializer::into_deserializer(
            s.to_ascii_lowercase(),
        ))
        .map(Some),
    }
}
