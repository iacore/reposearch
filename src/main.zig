const std = @import("std");
const httpz = @import("httpz");
const templates = @import("templates.zig");
const D = @import("concept").standard_dictionary;
const ref = @import("concept").ref;
const imagine = @import("concept").imagine;

test {
    // _ = templates;
}

const GlobalContext = struct {
    a: std.mem.Allocator,

    pub fn init(a: std.mem.Allocator) !@This() {
        return .{
            .a = a,
        };
    }
    pub fn deinit(ctx: *@This()) void {
        _ = ctx;
    }
};

var server: httpz.ServerCtx(*GlobalContext, *GlobalContext) = undefined;

fn interrupt(_: c_int, _: *const std.os.siginfo_t, _: ?*const anyopaque) callconv(.C) void {
    server.stop();
}

pub fn main() !void {
    var config = httpz.Config{};
    if (std.os.getenv("HOST")) |host| {
        config.address = host;
    }
    if (std.os.getenv("PORT")) |port| {
        config.port = try std.fmt.parseInt(u16, port, 10);
    }

    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const a = gpa.allocator();

    var ctx = try GlobalContext.init(a);
    defer ctx.deinit();

    server = try @TypeOf(server).init(a, config, &ctx);
    defer server.deinit();

    server.notFound(notFound);
    server.errorHandler(errorHandler);

    var router = server.router();

    router.get("/", indexPage);
    // router.post("/awawa", awawawa);

    inline for (.{ .TERM, .INT }) |signal| {
        std.os.sigaction(@field(std.os.SIG, @tagName(signal)), &.{ .handler = .{ .sigaction = &interrupt }, .mask = std.os.empty_sigset, .flags = 0 }, null) catch {};
    }

    {
        const listening_on_url = try std.fmt.allocPrint(a, "http://{s}:{}", .{ server.config.address.?, server.config.port.? });
        defer a.free(listening_on_url);

        imagine()
            .c(ref("event"), .{ D.self, D.do, "listen" })
            .c(null, .{ ref("event"), D.has, D.url, listening_on_url })
            .assert(ref("event"))
            .scream();
    }

    // start the server in the current thread, blocking.
    try server.listen();

    imagine()
        .c(ref("event"), .{ D.self, D.do, "exit" })
        .assert(ref("event"))
        .scream();
}

fn indexPage(_: *GlobalContext, req: *httpz.Request, res: *httpz.Response) !void {
    _ = req;

    // status code 200 is implicit.

    // you can set the body directly to a []u8, but note that the memory
    // must be valid beyond your handler. Use the res.arena if you need to allocate
    // memory for the body.
    res.body =
        \\(: "self" "says" "hello hello")
    ;
}

fn requestedHTML(req: *httpz.Request) bool {
    const accept = req.header("accept") orelse return false;
    var it = std.mem.split(u8, accept, ",");
    while (it.next()) |mime| {
        if (std.mem.eql(u8, mime, "text/html")) return true;
    }
    return false;
}

fn notFound(_: *GlobalContext, _: *httpz.Request, res: *httpz.Response) !void {
    res.status = 404;
    res.body =
        \\(: "request" "has" "status" "Not Found")
    ;
}

// note that the error handler return `void` and not `!void`
fn errorHandler(_: *GlobalContext, req: *httpz.Request, res: *httpz.Response, err: anyerror) void {
    res.status = 500;
    res.body =
        \\(: "request" "has" "status" "Internal Server Error")
    ;
    imagine()
        .c(ref("event"), .{ D.self, D.failed_at, ref("serving_request") })
        .c(null, .{ ref("serving_request"), D.has, D.url, req.url.raw })
        .c(null, .{ ref("serving_request"), D.has, D.error_code, err })
        .assert(ref("event"))
        .scream();
}
