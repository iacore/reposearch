.PHONY: dev build-release dist

dist: build-release
	rm -rf dist
	mkdir dist
	cp -r web/static dist/
	cp target/release/reposearch_web dist/

dev:
	cargo watch -i web/static -C web -cx 'run --bin reposearch_web'

build-release:
	cargo build --bin reposearch_web --release
