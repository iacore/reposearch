(: "unnamed" "has" "property" "concept capable")
(: "unnamed" "has" "ability" "self expression")

# some random code repository aggregate search engine

This is like SearX, but for git repos.

```sh
# Developing
make

# Buliding
cd web
cargo build --release

# Deploying
# Copy web/static/ and web/target/release/reposearch_web into a folder
```

## Why

The open source world is splintered. Repos are everywhere. With up-coming federation support in Forgejo, I need a search engine to search across all the sites.


## TODO

Meta

- [ ] choose a name for the project

Web frontend

- UI
    - [x] pagination
    - [x] enable columns based on provider
    - [x] download json
    - [x] JSON in Web Console
    - [ ] GithubSearchError, ForgejoSearchError should not be logged

- [ ] cli interface
    - [ ] json output
- [ ] stochastic ensemble (auto generate queries with similar semantics)
- [ ] cache search results and add to self (a smarter) search index

Backend

- [x] Github
    - [ ] login (OAuth). If not logged in, only first 1000 results are available
    - [ ] special search syntax
    - [ ] better error message
- [x] sr.ht
- [x] Forgejo (e.g. codeberg)
    - [x] custom API endpoint (Forgejo instance)
    - [ ] support more search options
    - [ ] login
    - [ ] tags? (need a separate request for each repo)
- [ ] Gitlab
  (unauthenticated search is disallowed on Gitlab)
- [ ] stackoverflow
- [ ] Sourceforge
- [ ] bitbucket

## Contribution: How to contribute site support

- find search API docs of that site
    - if no API docs, use unofficial HTTP API
- Add the api to `lib/src/<site_name>.rs` (See `lib/src/github.rs` for an example)
- minimally you need to support
    - free-form text search
    - pagination
