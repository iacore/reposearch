//! Scraping sr.ht
//! Will change when it has an API

use eyre::ContextCompat;
use lazy_static::lazy_static;
use regex::Regex;
use scraper::{ElementRef, Html, Selector};
use serde::Serialize;
use url::Url;

#[derive(strum::Display, Clone, Copy, Debug)]
pub enum SourcehutSortBy {
    #[strum(serialize = "recently-updated")]
    RecentlyUpdated,
    #[strum(serialize = "longest-active")]
    LongestActive,
}

#[derive(Debug)]
pub struct SourcehutSearchResult {
    pub page: u64,
    pub max_page: u64,
    pub repos: Vec<SourcehutRepoInfo>,
}

#[derive(Debug, Serialize)]
pub struct SourcehutTag {
    /// includes the leading `#`
    pub name: String,
    pub url: Url,
}

#[derive(Debug)]
pub struct SourcehutRepoInfo {
    pub user_url: Url,
    /// includes the leading `~`
    pub user_name: String,
    pub repo_url: Url,
    pub repo_name: String,
    pub description: Option<String>,
    pub tags: Vec<SourcehutTag>,
}

impl SourcehutRepoInfo {
    pub fn full_name(&self) -> String {
        format!("{}/{}", self.user_name, self.repo_name)
    }
}

lazy_static! {
    static ref URL_SEARCH: Url = Url::parse("https://sr.ht/projects").unwrap();
}

#[tracing::instrument]
pub async fn sourcehut_search_repos(
    term: &str,
    page: u64,
    sort_by: SourcehutSortBy,
) -> Result<SourcehutSearchResult, eyre::Report> {
    assert!(page >= 1);
    let client = reqwest::Client::builder()
        .build()?;
    let response = client
        .get(URL_SEARCH.as_str())
        .query(&[
            ("search", term),
            ("page", &page.to_string()), // todo: fetch different pages
            ("sort", &sort_by.to_string()),
        ])
        .header("User-Agent", crate::USER_AGENT)
        .header("Accept", "application/vnd.github+json")
        .header("X-GitHub-Api-Version", "2022-11-28")
        .send()
        .await?;

    let final_url = response.url();
    tracing::trace!(?final_url);

    let body = response.text().await?;
    tracing::trace!(?body, "received body");
    let body = Html::parse_document(&body);

    lazy_static! {
        static ref SEL_PAGE_COUNT: Selector = Selector::parse("div.col-4:nth-child(2)").unwrap();
        static ref SEL_REPO_INFO_CONTAINER: Selector = Selector::parse("form+.event-list").unwrap();
        static ref SEL_USER: Selector = Selector::parse("h4>a:nth-child(1)").unwrap();
        static ref SEL_REPO: Selector = Selector::parse("h4>a:nth-child(2)").unwrap();
        static ref SEL_DESC: Selector = Selector::parse("p").unwrap();
        static ref SEL_TAG: Selector = Selector::parse("a.tag").unwrap();
    }

    // e.g. `1 / 2`
    let page_count_str: Vec<_> = body.select(&SEL_PAGE_COUNT).collect();
    match page_count_str.len() {
        0 => {
            return Ok(SourcehutSearchResult {
                page,
                max_page: 0,
                repos: vec![],
            })
        }
        1 => {}
        _ => eyre::bail!("Can't find page number. This either means the scraper is outdated, or the returned HTML contains error message or something like that."),
    }
    let page_count_str = page_count_str[0];
    let page_count_str = page_count_str.inner_html();

    lazy_static! {
        static ref RE_PAGE_COUNT: Regex = Regex::new(r"(\d+)\s*/\s*(\d+)").unwrap();
    }

    let captures = RE_PAGE_COUNT
        .captures(&page_count_str)
        .with_context(|| format!("Can't parse page count. Str={:?}", page_count_str))?;
    let page = captures[1].parse()?;
    let max_page = captures[2].parse()?;

    let el_repos = body
        .select(&SEL_REPO_INFO_CONTAINER)
        .next()
        .context("Can't find repo info")?;

    let repos = el_repos
        .children()
        .into_iter()
        .filter(|el| el.value().is_element())
        .map(|el_repo| {
            let el_repo =
                ElementRef::wrap(el_repo).context("div.event-list should be an element")?;
            let a_user = el_repo
                .select(&SEL_USER)
                .next()
                .context("Can't find user link")?;
            let a_repo = el_repo
                .select(&SEL_REPO)
                .next()
                .context("Can't find repo link")?;
            let description = el_repo
                .select(&SEL_DESC)
                .next()
                .map(|el_desc| el_desc.inner_html());
            let tags = el_repo
                .select(&SEL_TAG)
                .map(|el_tag| {
                    let name = el_tag.inner_html();
                    let url = URL_SEARCH.join(
                        el_tag
                            .value()
                            .attr("href")
                            .context("Can't extract tag url")?
                            .into(),
                    )?;

                    Ok::<_, eyre::Report>(SourcehutTag { name, url })
                })
                .collect::<Result<_, _>>()?;

            let user_url = URL_SEARCH.join(
                a_user
                    .value()
                    .attr("href")
                    .context("Can't extract user_url")?
                    .into(),
            )?;
            let repo_url = URL_SEARCH.join(
                a_repo
                    .value()
                    .attr("href")
                    .context("Can't extract repo_url")?
                    .into(),
            )?;

            let user_name = a_user.inner_html();
            let repo_name = a_repo.inner_html();

            Ok::<_, eyre::Report>(SourcehutRepoInfo {
                user_url,
                user_name,
                repo_url,
                repo_name,
                description,
                tags,
            })
        })
        .collect::<Result<_, _>>()?;

    Ok(SourcehutSearchResult {
        page,
        max_page,
        repos,
    })
}
