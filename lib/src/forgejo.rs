//! https://codeberg.org/api/swagger#/repository/repoSearch
//! forgejo repo search API

use lazy_static::lazy_static;
use serde::Deserialize;
use url::Url;

#[derive(Debug, Deserialize, thiserror::Error)]
#[error("APIValidationError: {message}\n{url}")]
pub struct APIValidationError {
    pub message: String,
    pub url: String,
}

#[derive(Debug, Deserialize)]
pub struct SearchResults {
    pub ok: bool,
    pub data: Vec<Repository>,
}

#[derive(Debug, Deserialize)]
pub struct Repository {
    pub id: u64,
    pub full_name: String,
    pub description: String,
    pub html_url: Url,
    pub stars_count: u64,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub updated_at: chrono::DateTime<chrono::Utc>,
}

lazy_static! {
    pub static ref APIBASE_CODEBERG: Url = Url::parse("https://codeberg.org/api/v1/").unwrap();
}

#[tracing::instrument]
pub async fn search_repos(
    api_base: &Url,
    query: &str,
    page: u64,
) -> Result<SearchResults, eyre::Report> {
    eyre::ensure!(
        api_base
            .path_segments()
            .and_then(|x| x.last())
            .expect("api_base must have path")
            .len()
            == 0,
        "api_base must ends with a slash"
    );
    let client = reqwest::Client::builder().build()?;
    let response = client
        .get(api_base.join("repos/search")?)
        .header("accept", "application/json")
        .query(&[("q", query), ("page", &page.to_string())])
        .send()
        .await?;
    tracing::trace!(?response);
    match response.status().as_u16() {
        200 => {
            let results: SearchResults = response.json().await?;
            Ok(results)
        }
        422 => {
            let err: APIValidationError = response.json().await?;
            Err(err.into())
        }
        _ => {
            return Err(ForgejoSearchError(response).into());
        }
    }
}


#[derive(thiserror::Error, Debug)]
#[error("ForgejoSearchError: {0:?}")]
pub struct ForgejoSearchError(reqwest::Response);