//! https://docs.github.com/en/rest/search/search?apiVersion=2022-11-28#search-repositories

use super::*;
use eyre::Context;
use serde::Deserialize;
use serde_json::Value as JSONValue;
use url::Url;

// Warning: don't rename fields of structs labeled with `Deserialize`

#[derive(Debug, Deserialize)]
pub struct GithubOwnerInfo {
    pub avatar_url: Url,
}

#[derive(Debug, Deserialize)]
pub struct GithubLicenseInfo {
    pub spdx_id: String,
    pub name: String,
    pub url: Option<Url>,
}

#[derive(Debug, Deserialize)]
pub struct GithubRepoInfo {
    /// "author_name/repo_name"
    pub full_name: String,
    pub description: Option<String>,
    pub owner: GithubOwnerInfo,
    pub license: Option<GithubLicenseInfo>,
    pub pushed_at: Option<chrono::DateTime<chrono::Utc>>, // not sure what this is for
    pub updated_at: Option<chrono::DateTime<chrono::Utc>>, // last updated

    /// despite being called one, it's not a URL. e.g. `git@example.com:repo.git`
    pub ssh_url: String,
    pub html_url: Url,

    pub open_issues_count: u64,
    pub stargazers_count: u64,
    // watchers_count: u64, // this seems to be bugged to be equal to stargazers_count
    pub forks_count: u64,
}

#[derive(Deserialize, Debug)]
pub struct GithubSearchResult {
    pub total_count: u64,
    pub items: Vec<GithubRepoInfo>,
}

impl GithubSearchResult {
    pub fn max_page(&self) -> u64 {
        // ceil_div
        self.total_count / PER_PAGE_COUNT
            + (if self.total_count % PER_PAGE_COUNT == 0 {
                1
            } else {
                0
            })
    }
}

const PER_PAGE_COUNT: u64 = 100;
#[tracing::instrument]
pub async fn github_search_repos(
    term: &str,
    page: u64,
) -> Result<GithubSearchResult, eyre::Report> {
    assert!(page >= 1);

    let client = reqwest::Client::builder()
        .build()?;
    let response = client
        .get("https://api.github.com/search/repositories")
        .query(&[
            ("q", term),
            ("per_page", &PER_PAGE_COUNT.to_string()),
            ("page", &page.to_string()), // todo: fetch different pages
        ])
        .header("User-Agent", USER_AGENT)
        .header("Accept", "application/vnd.github+json")
        .header("X-GitHub-Api-Version", "2022-11-28")
        .send()
        .await?;
    let final_url = response.url();
    tracing::trace!(?final_url);

    let results: JSONValue = response.json().await?;
    tracing::trace!(?results);
    if results.get("errors").is_some() {
        return Err(GithubSearchError(results).into());
    }
    let ans = GithubSearchResult::deserialize(&results).with_context(|| results)?;
    Ok(ans)
}

#[derive(thiserror::Error, Debug)]
#[error("GithubSearchError: {0}")]
pub struct GithubSearchError(JSONValue);
