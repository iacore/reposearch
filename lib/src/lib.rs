pub mod github;
pub mod srht;
pub mod forgejo;

/// unparsed string that should be an URL
pub type UrlString = String;

pub const USER_AGENT: &str = "meta repo search engine thingy";
